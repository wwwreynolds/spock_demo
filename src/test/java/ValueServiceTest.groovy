import com.bobr.demo.spock.ValueBean
import com.bobr.demo.spock.ValueRepository
import com.bobr.demo.spock.ValueService
import spock.lang.Specification
import spock.lang.Subject
import spock.lang.Unroll

/**
 * This class describes basic features of Spock tests.
 *
 * @see "http://spockframework.org/spock/docs/1.0/spock_primer.html"
 */

// In spock, tests are called specifications and must extend the Specification class.
class ValueServiceTest extends Specification {

    // This field is used to mock the ValueRepository class
    def valueRepository = Mock(ValueRepository)

    @Subject // indicates that this field is the object-under-test (has no functional value)
    ValueService service = new ValueService(valueRepository: valueRepository)

    // run before every test (optional)
    def given() {
    }

    // run after every test (optional)
    def cleanup() {}

    // run before the first test (optional)
    def setupSpec() {}

    // run after the last test (optional)
    def cleanupSpec() {}

    // in spock, test methods are called 'feature methods'
    def "spock has blocks"() {
        //
        // In spock, you write your test code inside labeled 'blocks' that describe the 'flow' of the test.
        //

        // A given block, which is where you set things up (similar to "setup")
        given: "when value is two"
        def value = 2

        // "and" blocks allow you to break up other blocks into pieces and can be used within "given", "when", "then", etc.
        and:
        def bean = new ValueBean(value: value)

        // This is the test 'stimulus' block
        when:
        def result = service.valueSquared(bean)

        // This is where you verify results. All statements resulting in "boolean" must be true.
        then:
        result == 4
    }

    def "conditions"() {
        // "expect" is similar to 'then' block, but w/o 'when' block
        expect:
        true
        new Integer(1)
        ! new Integer(0) // zero is 'false' according to groovy truth
        "s"
        ! ""             // empty string evaluates to false
        ! null           // null evaluates to false
        [1]              // array evaluates to true
        ! []             // empty array evaluates to false
    }

    def "stubbing"() {
        given:
        def stubbed = Stub(ValueBean)

        and:
        // when getValue is called, return 2
        stubbed.getValue() >> 2

        expect:
        service.valueSquared(stubbed) == 4
    }

    // Mocking is like stubbing with the added feature of being able to verify interractions.
    def "mocking"() {
        given:
        def bean = new ValueBean(value: 2)

        when:
        service.store(bean)

        then:
        // This is a combination of verifying interaction and stubbing a result. This mock uses the wildcard
        // Spock recommends that '1 *' be placed in the 'then:' block for better readability.
        1 * valueRepository.store(_) >> new ValueBean(value: 2)
    }

    def "parameter matching"() {
        given:
        def bean = new ValueBean(value: 2)

        when:
        service.store(bean)

        then:
        // match call to store with one parameter with any value
        1 * valueRepository.store(_) >> new ValueBean(value: 2)

        when:
        service.store(bean)

        then:
        // match call to store with any number of parameters with any value
        1 * valueRepository.store(*_) >> new ValueBean(value: 2)

        when:
        service.store(bean)

        then:
        // use closure that returns true when the expected value is passed
        1 * valueRepository.store({it.value == 2}) >> new ValueBean(value: 2)
    }

    def "parameterized test"() {
        given:
        def bean = new ValueBean(value: value)

        expect:
        service.valueSquared(bean) == expectedResult

        where:
        value | expectedResult
        1     | 1
        2     | 4
        3     | 9
    }

    @Unroll
    def "parameterized test when value=#value - result is #expectedResult"() {
        given:
        def bean = new ValueBean(value: value)

        expect:
        service.valueSquared(bean) == expectedResult

        where:
        value | expectedResult
        1     | 1
        2     | 4
        3     | 9
    }

    def "exception test"() {
        given:
        def bean = new ValueBean(value: 2)

        and:
        valueRepository.store(_) >> new ValueBean(value: 1)

        when:
        service.store(bean)

        then:
        thrown IllegalStateException
    }

    // Do not fix this test
    def "this test fails"() {
        given:
        def bean = new ValueBean(value: 2)

        expect:
        // See the output of this test for the nicely formatted difference between the expected and actual result
        bean == new ValueBean(value: 1)
    }

}