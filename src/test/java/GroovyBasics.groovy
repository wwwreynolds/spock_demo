import com.bobr.demo.spock.ValueBean
import spock.lang.Specification

/**
 * This class describes basic features of the groovy language.
 *
 * @see "http://groovy-lang.org/differences.html"
 */

// Tests must extend Specification
class GroovyBasics extends Specification {

    // In groovy, function names can be any valid string, including spaces and punctuation so your
    // test names can be very descriptive.
    private def "this is a function"() {
    }

    // But, standard java names are valid too
    def thisIsAFunction() {
    }

    def "groovy is about simplified syntax"() {
        // use def to let Groovy figure out the type of a variable
        def i = new Integer(1)
        def j = Integer.valueOf("1") // groovy figures out the type by the return type of valueOf()
        def k = functionThatReturnsAnInteger() // groovy assumes Integer from the returned value of this function

        // semicolons are optional
        println("hello w/o semicolon")
        println("hello");

        // parenthesis are optional
        println "hello w/o parenthesis"

        // System.out is optional
        System.out.println("hello")
        println("hello")
    }

    // groovy figures out the return type by the variable being returned (i.e., Integer)
    def functionThatReturnsAnInteger() {
        // no need for "return".  The last statement is the returned value
        new Integer(1)
    }

    def "== operator is deep equal"() {
        setup:
        def x = new Integer(0)
        def y = new Integer(0)

        expect:
        // in groovy, == is implemented using .equals
        x == y
        x.equals(y)

        // for identity equals, use .is
        !x.is(y)
        x.is(x)
    }

    def "cool beans"() {
        setup:
        // you can combine creating and setting bean properties in one call
        def bean = new ValueBean(value: 0)

        expect:
        // these are equivalent.
        // properties accessed via .getXxx() are available via normal property syntax (.xxx).
        bean.value == 0
        bean.getValue() == 0

        when:
        // these are equivalent
        bean.value = 1 // setValue(1) is actually called here
        bean.setValue(1)

        then:
        bean.value == 1
    }

    def "arrays"() {
        setup:
        // in groovy, {...} is reserved for closures, so you use [...]
        def a = [0, 1, 2]

        when:
        // append operator
        a << 3

        then:
        a == [0, 1, 2, 3]

        when:
        a = 2..5 // array containing sequence of numbers 2, 3, 4, 5

        then:
        a == [2, 3, 4, 5]
    }

    def "maps"() {
        setup:
        // in groovy, maps are constructed using colon (:) operator
        def a = [first: 1, second: 2]

        expect:
        // lookup using []
        a["first"] == 1
        // lookup using .
        a.first == 1
        // lookup using Map.get
        a.get("first") == 1
    }

    def "closures"() {
        setup:
        def a = [1, 2]

        expect:
        //
        // Closures are like java 8 lambdas and are delimited by {...}.
        //

        // create a new array by multiplying each element by 100. The variable 'it' inside the closure refers
        // to the element being iterated.
        a.collect{it * 100} == [100, 200]

        // the closure is passed as an argument to the collect() function. Remember, parenthesis are optional.
        a.collect({it * 100}) == [100, 200]

        // you can name closure variables
        a.collect{x -> x * 100} == [100, 200]

        // convert list to map. Uses map syntax [:] inside the closure
        a.collectEntries {[("number$it" as String) : it * 100]} == [number1 : 100, number2: 200]

        // careful with gStrings. The following is identical to the previous except cast is excluded so the maps
        // are NOT equal!
        a.collectEntries {["number$it" : it * 100]} != [number1 : 100, number2: 200]
    }

    def "closures on collections"() {
        setup:
        def a = [1, 2]

        expect:
        //
        // Groovy provides a rich set of methods that take closures
        //

        // test that at least one element matches an expression
        a.any{it == 1}

        // test that ALL elements in the collection match an expression
        a.every{it > 0}

        // find the first element matching expression
        a.find{it == 1} == 1

        // find all elements matching expression
        a.findAll{it > 0} == [1, 2]

        // count matching
        a.count{it > 0} == 2
        a.count{it > 100} == 0

        // prints to console
        // 1
        // 2
        a.each{println it}
    }

    def "groovy strings (aka gstrings)"() {
        setup:
        def x = 0

        // in spock, every statement in expect (and 'then') block must evaluate to 'true' or the test fails
        expect:
        // groovy strings use double quotes and can have embedded expressions using '$'
        "embedded $x" == "embedded 0"

        // IDE can be a bit annoyed w/ gstrings. Sometimes you'll need to cast to avoid the warning.
        ((String) "embedded $x") == "embedded 0"
        "embedded $x" as String == "embedded 0"

        // a more complicated expression, note curly braces
        "embedded ${x + 2}" == "embedded 2"

        // literal strings use single quote characters and do not expand embedded expressions
        'literal $x' == "literal \$x"
    }

    def "cast operators"() {
        setup:
        def x = new Integer(0)

        // groovy supports many cast operators
        expect:
        x as String == "0"
        "0" as Integer == x
    }

    def "you can use standard java"() {
        //
        // in groovy, you can almost always fall back to standard java syntax
        //
    }
}