package com.bobr.demo.spock;

public interface ValueRepository {
    public ValueBean store(ValueBean valueBean);
}
