package com.bobr.demo.spock;

import java.util.Objects;

public class ValueBean {
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "ValueBean{" +
                "value=" + value +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ValueBean valueBean = (ValueBean) o;
        return getValue() == valueBean.getValue();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }
}
