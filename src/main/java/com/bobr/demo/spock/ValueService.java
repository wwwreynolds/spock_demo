package com.bobr.demo.spock;

import lombok.Data;

@Data
public class ValueService {
    private ValueRepository valueRepository;

    public int valueSquared(ValueBean valueBean) {
        return valueBean.getValue() * valueBean.getValue();
    }

    public void store(ValueBean valueBean) {
        ValueBean storedValue = valueRepository.store(valueBean);
        if (storedValue.getValue() != valueBean.getValue()) {
            throw new IllegalStateException("stored value does not match");
        }
    }
}
